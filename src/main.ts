import { createApp } from "vue";
import { createPinia } from "pinia";
import NothingUi from "nothing-ui";
import "nothing-ui/dist/style.css";
import "./style.css";
import Router from "./router";
import App from "./App.vue";

const pinia = createPinia();
const app = createApp(App);
app.use(NothingUi);
app.use(pinia);
app.use(Router);
app.mount("#app");
