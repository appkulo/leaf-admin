/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{vue,js,ts,jsx,tsx}"],
  corePlugins: {
    preflight: true,
  },
  theme: {
    extend: {
      colors: {
        primary: '#4f5d77',
        lightGrayLeaf: '#f8f9fa',
        grayLeaf: '#6c757d',
        greenBody: '#3BB77E',
      }
    },
  },
  plugins: [],
};
